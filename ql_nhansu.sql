-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 03, 2019 lúc 12:07 PM
-- Phiên bản máy phục vụ: 10.3.16-MariaDB
-- Phiên bản PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ql_nhansu`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhansu`
--

CREATE TABLE `nhansu` (
  `ID` int(11) NOT NULL,
  `maNV` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `ho` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gioitinh` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `quoctich` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dantoc` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tongiao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `soCMND` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ngaycap` date NOT NULL,
  `noicap` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `anh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `didong` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quocgia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tinhthanh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quanhuyen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phuongxa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ttkhac` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `honnhan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chieucao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nhommau` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `xuatthan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sothich` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `hocvan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ngoaingu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `daotao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nganh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `moiquanhe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hoten` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `diachi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nhansu`
--

INSERT INTO `nhansu` (`ID`, `maNV`, `ho`, `ten`, `gioitinh`, `ngaysinh`, `quoctich`, `dantoc`, `tongiao`, `soCMND`, `ngaycap`, `noicap`, `anh`, `didong`, `email`, `skype`, `facebook`, `quocgia`, `tinhthanh`, `quanhuyen`, `phuongxa`, `ttkhac`, `honnhan`, `chieucao`, `nhommau`, `xuatthan`, `sothich`, `hocvan`, `ngoaingu`, `daotao`, `nganh`, `moiquanhe`, `hoten`, `diachi`, `state`) VALUES
(1, 'NV01', 'Nguyễn', 'Hạnh', 'Nữ', '1997-11-10', 'Việt Nam', 'Kinh', 'Không', '', '2013-07-19', 'Công an Quảng Trị', 'avatar.jpg', '0399475991', 'hanhnguyen060497@gmail.com', 'hanhnguyen', 'hanhsino', 'Việt Nam', 'Quảng Trị', 'Huyện Gio Linh', 'Xã Gio Quang', 'kt', 'Độc thân', '1m6', 'O', '', 'ăn', '', 'Anh', 'Đại học', 'Công nghệ thông tin', '[\"Mẹ\"]', '[\"Hoàng  N\"]', '[\"Quảng Trị\"]', ''),
(2, 'NV02', 'Hồ', 'Hoàng', 'Nam', '2019-06-16', 'Việt Nam', '', '', '', '0000-00-00', '', 'avatar5.png', '09207332', 'hoangmathk39@gmail.com', '', '', 'Việt Nam', 'Thừa Thiên Huế', 'Thành Phố Huế', 'Phường An Cựu', '', 'Độc thân', '1m73', 'B', '', '', '', 'Anh', 'Đại học', 'Toán', '[\"Mẹ\",\"Cha\"]', '[\"Nguyễn Hương\",\"Hồ MInh\"]', '[\"Huế\",\"Huế\"]', ''),
(9, 'NV10', 'Lê', 'My', 'Nữ', '1993-07-12', 'Việt Nam', 'Kinh', 'Không', '', '0000-00-00', '', 'avatar2.png', '09207332', 'sdfgd@gmail.com', '', '', 'Việt Nam', 'Cao Bằng', 'Huyện Thông Nông', 'Xã Cần Yên', '', 'Độc thân', '1m50', 'O', '', '', '', '', '', '', '[\"Mẹ\"]', '[\"Lê Hiền\"]', '[\"Cao Bằng\"]', ''),
(16, 'NV14', 'Lê', 'Hi', 'Nam', '1995-07-20', 'Việt Nam', 'Kinh', 'Không', '', '0000-00-00', '', 'avatar04.png', '09207332', 'gsjkhldssd@gmail.com', 'adsa', 'LeHi', 'Việt Nam', 'Phú Thọ', 'Huyện Đoan Hùng', 'Thị Trấn Đoan Hùng', '', 'Độc thân', '', '', '', '', '', '', 'Cao Đẳng', 'Tài chính-kinh tế', '[\"Cha\"]', '[\"Lê BÌnh\"]', '[\"Phú Thọ\"]', ''),
(18, 'NV15', 'Hồ', 'My', 'Nữ', '1993-07-17', 'Việt Nam', 'Kinh', 'Phật Giáo', '', '2019-07-26', 'Thừa Thiên Huế', 'avatar3.png', '09207332', 'adfgh@gmail.com', 'ewrt', 'fghdqerty', 'Việt Nam', 'Hà Nội', 'Huyện Thanh Trì', 'Xã Tả Thanh Oai', '', 'Độc thân', '1m60', 'O', '', '', '', 'Anh,Nhật', 'Thạc Sỹ', 'Công nghệ thông tin', '[\"Mẹ\"]', '[\"TRần Q\"]', '[\"Huế\"]', ''),
(19, 'NV17', 'Lê', 'Nam', 'Nam', '1992-07-20', 'Việt Nam', 'Kinh', 'Không', '', '2012-07-18', 'Công an Đà Nẵng', 'user1-128x128.jpg', '09207332', 'sbndbj@gmail.com', '', '', 'Chọn', 'Chọn', 'Chọn', 'Chọn', '12 Phan Bội Châu', 'Độc thân', '', '', '', '', '', '', '', '', '[\"Mu1eb9\"]', '[\"Tru1ea7n Thu1ecb Hu00e0\"]', '[\"u\"]', ''),
(21, 'NV18', 'Trần', 'Anh', 'Khác', '2019-07-24', 'Việt Nam', 'Kinh', 'Không', '', '0000-00-00', '', 'user8-128x128.jpg', '09207332', 'hanhnguyen060497@gmail.com', '', '', 'Việt Nam', 'Bắc Ninh', 'Huyện Quế Võ', 'Xã Phương Liễu', '', 'Độc thân', '', '', '', '', '', '', '', '', '[\"Cha\",\"Mẹ\"]', '[\"TRần Q\",\"Hoàng K\"]', '[\"Quế Võ-Bắc Ninh\",\"Quế Võ-Bắc Ninh\"]', ''),
(23, 'VN19', 'Trần', 'Bảo Bảo', 'Khác', '1990-07-12', 'Việt Nam', '', '', '', '0000-00-00', '', 'user6-128x128.jpg', '09207332', 'bbtran@gmail.com', 'bbtran', 'Trần Bảo Bảo', 'Việt Nam', 'Hồ Chí Minh', 'Quận Thủ Đức', 'Phường Tam Bình', '', 'Độc thân', '1m83', 'O', '', '', '', 'Anh,Pháp', 'Trung cấp', 'Điện tử viễn thông', '[\"Cha\",\"Mẹ\"]', '[\"Trần Bảo Minh\",\"Lê Giang\"]', '[\"TP Hồ Chí  Minh\",\"TP Hồ Chí  Minh\"]', ''),
(24, 'NV20', 'Lê', 'Sao Mai', 'Nữ', '1992-07-18', 'Việt Nam', '', '', '', '2019-07-03', '', 'user7-128x128.jpg', '09207332', 'ffff497@gmail.com', '', '', 'Chọn', 'Chọn', 'Chọn', 'Chọn', '145 Đống Đa', 'Độc thân', '1m6', '', '', '', '', '', '', '', '[\"Cha\",\"me\"]', '[\"t\",\"y\"]', '[\"H\",\"u\"]', ''),
(25, 'NV11', 'Nguyễn', 'Châu', 'Nam', '2019-07-11', 'Việt Nam', 'Kinh', 'Không', '', '2012-07-17', 'Thừa Thiên Huế', 'user3-128x128.jpg', '09207332', 'dfhgfj@gmail.com', '', '', 'Chọn', 'Chọn', 'Chọn', 'Chọn', '', 'Độc thân', '1m73', 'A', '', '', 'Cao Đẳng', '', '', '', '[\"Cha\",\"Mẹ\",\"Vợ\",\"Cn\"]', '[\"\",\"\",\"\",\"\"]', '[\"Đà Nẵng\",\"Đà Nẵng\",\"HUế\",\"Huế\"]', ''),
(27, 'NV03', 'Lê', 'Vân', 'Nữ', '1992-07-11', 'Việt Nam', 'Kinh', 'Không', '', '0000-00-00', '', '', '09207332', 'vangdd@gmail.com', '', '', 'Việt Nam', 'Đà Nẵng', 'Quận Sơn Trà', 'Phường Nại Hiên Đông', '', 'Độc thân', '', '', '', '', '', '', '', '', '[\"Cha\",\"Mẹ\"]', '[\"Lê D\",\"Hoàng N\"]', '[\"Đà Nẵng\",\"Đà Nẵng\"]', ''),
(28, 'NV09', 'Ngô', 'Hân', 'Nữ', '1993-07-10', 'Việt Nam', '', '', '', '0000-00-00', '', 'user5-128x128.jpg', '09207332', 'reer39@gmail.com', '', '', 'Việt Nam', 'Nam Định', 'Huyện Nghĩa Hưng', 'Xã Nghĩa Minh', '', 'Độc thân', '', '', '', '', '', '', '', '', '[\"Cha\",\"Mẹ\"]', '[\"Ngô Long\",\"Lê Hoa\"]', '[\"Huế\",\"Huế\"]', '');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `nhansu`
--
ALTER TABLE `nhansu`
  ADD PRIMARY KEY (`ID`,`maNV`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `nhansu`
--
ALTER TABLE `nhansu`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
