<?php
  require('connect.php');
  
  if($_SERVER["REQUEST_METHOD"]=="POST"){
    $data['maNV']=isset($_POST['maNV'])? $_POST['maNV']:'';
    $data['ho']=isset($_POST['ho'])? $_POST['ho']:'';
    $data['ten']=isset($_POST['ten'])? $_POST['ten']:'';
    $data['gioitinh']=isset($_POST['gioitinh'])? $_POST['gioitinh']:'';
    $data['ngaysinh']=isset($_POST['ngaysinh'])? $_POST['ngaysinh']:'';
    $data['quoctich']=isset($_POST['quoctich'])? $_POST['quoctich']:'';
    $data['dantoc']=isset($_POST['dantoc'])? $_POST['dantoc']:'';
    $data['tongiao']=isset($_POST['tongiao'])? $_POST['tongiao']:'';
    $data['soCMND']=isset($_POST['soCMND'])? $_POST['soCMND']:'';
    $data['ngaycap']=isset($_POST['ngaycap'])? $_POST['ngaycap']:'';
    $data['noicap']=isset($_POST['noicap'])? $_POST['noicap']:'';
    $data['anh']=isset($_POST['anh'])? $_POST['anh']:'';
    $data['didong']=isset($_POST['didong'])? $_POST['didong']:'';
    $data['email']=isset($_POST['email'])? $_POST['email']:'';
    $data['skype']=isset($_POST['skype'])? $_POST['skype']:'';
    $data['facebook']=isset($_POST['facebook'])? $_POST['facebook']:'';
    $data['quocgia']=isset($_POST['quocgia'])? $_POST['quocgia']:'';
    $data['tinhthanh']=isset($_POST['tinhthanh'])? $_POST['tinhthanh']:'';
    $data['quanhuyen']=isset($_POST['quanhuyen'])? $_POST['quanhuyen']:'';
    $data['phuongxa']=isset($_POST['phuongxa'])? $_POST['phuongxa']:'';
    $data['ttkhac']=isset($_POST['ttkhac'])? $_POST['ttkhac']:'';
    $data['honnhan']=isset($_POST['honnhan'])? $_POST['honnhan']:'';
    $data['chieucao']=isset($_POST['chieucao'])? $_POST['chieucao']:'';
    $data['nhommau']=isset($_POST['nhommau'])? $_POST['nhommau']:'';
    $data['xuatthan']=isset($_POST['xuatthan'])? $_POST['xuatthan']:'';
    $data['sothich']=isset($_POST['sothich'])? $_POST['sothich']:'';
    $data['hocvan']=isset($_POST['hocvan'])? $_POST['hocvan']:'';
    $data['ngoaingu']=isset($_POST['ngoaingu'])? $_POST['ngoaingu']:'';
    $data['daotao']=isset($_POST['daotao'])? $_POST['daotao']:'';
    $data['nganh']=isset($_POST['nganh'])? $_POST['nganh']:'';
   
 
   
    $data['moiquanhe']=json_encode(isset($_POST['moiquanhe'])? $_POST['moiquanhe']:'',JSON_UNESCAPED_UNICODE);
    

    $data['hoten']=json_encode(isset($_POST['hoten'])? $_POST['hoten']:'',JSON_UNESCAPED_UNICODE);
  
    $data['diachi']=json_encode(isset($_POST['diachi'])? $_POST['diachi']:'',JSON_UNESCAPED_UNICODE);
    





    $errors=array();
    if(empty($data['maNV'])){
      $errors['maNV']="Không được bỏ trống mã nhân viên ";
    }
    if(!empty(getNVByMa($data['maNV']))){
      $errors['maNV']="Mã nhân viên đã tồn tại";
    }
   
    if(empty($data['ho'])){
      $errors['ho']="Không được bỏ trống Họ";
    }
    if(empty($data['ten'])){
      $errors['ten']="Không được bỏ trống tên ";
    }
    if(empty($data['ngaysinh'])){
      $errors['ngaysinh']="Không được bỏ trống ngày sinh ";
    }
    if(empty($data['email'])){
      $errors['email']="Không được bỏ trống email ";
    }
    if(empty($data['quoctich'])){
      $errors['quoctich']="Không được bỏ trống quốc tịch ";
    }
    if(empty($data['didong'])){
      $errors['didong']="Không được bỏ trống di động ";
    }
    if(empty($data['gioitinh'])){
      $errors['gioitinh']="Không được bỏ trống giới tính ";
    }
    if(empty($data['anh'])){
      $errors['anh']="Không được bỏ trống ảnh cá nhân ";
    }

  
    if(!$errors){
     
      addNhanVien($data['maNV'],$data['ho'],$data['ten'],$data['gioitinh'],$data['ngaysinh'],
      $data['quoctich'],$data['dantoc'],$data['tongiao'],
      $data['soCMND'],$data['ngaycap'],$data['noicap'],$data['anh'],$data['didong'],$data['email'],
      $data['skype'],$data['facebook'],
      $data['quocgia'],$data['tinhthanh'],$data['quanhuyen'],$data['phuongxa'],$data['ttkhac'],
      $data['honnhan'],$data['chieucao'],$data['nhommau'],
      $data['xuatthan'],$data['sothich'],$data['hocvan'],$data['ngoaingu'],$data['daotao'],
      $data['nganh'],$data['moiquanhe'],$data['hoten'],
      $data['diachi']);
   
  
      header('location: index.php');
    }


  }
  getDisconnect();


?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Thêm nhân sự </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">

  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/styles.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/avatar.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Hạnh Nguyễn</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

               
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/avatar.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Hạnh Nguyễn</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      
  
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"></li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản lý nhân sự</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="index.php"><i class="fa fa-circle-o"></i> Danh sách nhân sự</a></li>
            <li class="active"><a href="index1.php"><i class="fa fa-circle-o"></i> Thêm nhân sự</a></li>
            
            
          </ul>
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <style>
    .error{ color:red;}
    input.radio-btn {  margin: 10px;}
  </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Thêm nhân sự
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thêm nhân sự</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
   
     
            <form action="index1.php" method="post" accept-charset="utf-8"> 
              <div class="box box-primary box-solid">
                <div class="box-header with-border">
                <h3 class="box-title">Thông tin chung</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                
                      <div class="form-group col-md-4">
                      <label for="inputAddress">Mã nhân viên</label>  <span class="error">* <?php if (!empty($errors['maNV'])) echo $errors['maNV']; ?></span>
                      <input type="text" class="form-control" id="maNV" name="maNV" placeholder="Nhập mã nhân viên" value="<?php echo !empty($data['maNV']) ? $data['maNV'] : '';?>">
                      
                     </div>
                    <div class="form-group col-md-4">
                     <label for="inputAddress">Họ</label>
                     <span class="error">* <?php if (!empty($errors['ho'])) echo $errors['ho']; ?></span>
                     <input type="text" name="ho" class="form-control" id="ho" placeholder="Nhập họ" value="<?php echo !empty($data['ho']) ? $data['ho'] : '';?>">
                     
                   </div>
                    <div class="form-group col-md-4">
                      <label for="inputAddress">Tên</label>
                      <span class="error">* <?php if (!empty($errors['ten'])) echo $errors['ten']; ?></span>
                      <input type="text" class="form-control"name="ten" id="ten" placeholder="Nhập tên" value="<?php echo !empty($data['ten']) ? $data['ten'] : '';?>">
                      
                   </div>
                   <div class="form-group col-md-4">
                      <label for="inputState">Giới tính</label>
                      <span class="error">* <?php if (!empty($errors['gioitinh'])) echo $errors['gioitinh']; ?></span>
                      <br>
                      <input class="radio-btn" type="radio" name="gioitinh" value="Nam" <?php if(isset($data['gioitinh']) && $data['gioitinh']=="Nam") echo "checked " ?>>Nam
                      <input   class="radio-btn" type="radio" name="gioitinh" value="Nữ" <?php if(isset($data['gioitinh']) && $data['gioitinh']=="Nữ")  echo "checked " ?>>Nữ
                      <input  class="radio-btn" type="radio" name="gioitinh" value="Khác" <?php if(isset($data['gioitinh']) && $data['gioitinh']=="Khác")  echo "checked " ?>>Khác
                     
                     
                   </div>
                 
                  <div class="form-group col-md-4">  
                    <label for="inputAddress">Ngày sinh</label>
                    <span class="error">* <?php if (!empty($errors['ngaysinh'])) echo $errors['ngaysinh']; ?></span>
                    <input type="date" class="form-control" name="ngaysinh" placeholder="Ngày Ngày sinh" value="<?php echo !empty($data['ngaysinh']) ? $data['ngaysinh'] : '';?>">
                  
                  </div> 
                   <div class="form-group col-md-4">
                   <label for="inputAddress">Quốc tịch</label>
                   <span class="error">* <?php if (!empty($errors['quoctich'])) echo $errors['quoctich']; ?></span>
                    <input type="text" class="form-control" name="quoctich" placeholder="Nhập Quốc tịch" value="<?php echo !empty($data['quoctich']) ? $data['quoctich'] : '';?>">
                    

                  </div>
                 
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Dân tộc</label>
                    <input type="text" class="form-control" name="dantoc" placeholder="Nhập Dân tộc"  value="<?php echo !empty($data['dantoc']) ? $data['dantoc'] : '';?>">

                  </div>
                 
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Tôn Giáo</label>
                    <input type="text" class="form-control" name="tongiao" placeholder="Nhập Tôn Giáo" value="<?php echo !empty($data['tongiao']) ? $data['tongiao'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-4">
                  
                    <label for="inputAddress">Số CMND</label>
                   
                    <input type="text" class="form-control" name="soCMND " placeholder="Nhập số  CMND"  value="<?php echo !empty($data['soCMND']) ? $data['soCMND'] : '';?>">
                    
                  
                  </div>

                    <div class="form-group col-md-4">
                    <label for="inputAddress">Ngày cấp</label>
                    <input type="date" class="form-control" name="ngaycap"  value="<?php echo !empty($data['ngaycap']) ? $data['ngaycap'] : '';?>">
                  </div>
                  
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Nơi cấp</label>
                    <input type="text" class="form-control" name="noicap" placeholder="Nhập nơi cấp"  value="<?php echo !empty($data['noicap']) ? $data['noicap'] : '';?>">
                  </div>
                  
                    <div class="form-group col-md-4">
                      <label for="inputZip">Ảnh cá nhân</label>
                      <span class="error">* <?php if (!empty($errors['anh'])) echo $errors['anh']; ?></span>
                      <input type="file" class="form-control" name="anh"  value="<?php echo !empty($data['anh']) ? $data['anh'] : '';?>">
                    </div>
                 </div>
                
               
              </div>
              <!-- /.box-body -->
               <div class="box box-primary box-solid">
                <div class="box-header with-border">
                <h3 class="box-title">Thông tin liên lạc</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                 
                       <div class="form-group col-md-6">
                      <label for="inputAddress">Di động</label>
                      <span class="error">* <?php if (!empty($errors['didong'])) echo $errors['didong']; ?></span>
                      <input type="text" class="form-control" name="didong" placeholder=""  value="<?php echo !empty($data['didong']) ? $data['didong'] : '';?>">
                     
                    </div>
                      <div class="form-group col-md-6 ">
                    
                        <label for="inputEmail4">Email</label>
                        <span class="error">* <?php if (!empty($errors['email'])) echo $errors['email']; ?></span>
                             
                        <input type="email" class="form-control" name="email" placeholder="Email"  value="<?php echo !empty($data['email']) ? $data['email'] : '';?>">
                       
                      </div>
                       <div class="form-group col-md-6">
                      <label for="inputAddress2">Skype</label>
                      <input type="text" class="form-control" name="skype" placeholder=""  value="<?php echo !empty($data['skype']) ? $data['skype'] : '';?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label for="inputAddress">Facebook</label>
                      <input type="text" class="form-control" name="facebook" placeholder=""  value="<?php echo !empty($data['facebook']) ? $data['facebook'] : '';?>">
                    </div>
                    
                     <div class="form-group col-md-12"><h4> Địa chỉ cư trú:</h4></div>
                   
                    <div class="form-group col-md-4">
                        <label for="inputState">Quốc gia</label>
                        <select id="quocgia"  name="quocgia" class="form-control select2 " label= "<?php echo !empty($data['quocgia']) ? $data['quocgia'] : '';?>">
                        <option>Chọn</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">Tỉnh/thành</label>
                        <select id="tinh" name="tinhthanh"class="form-control select2 "label="<?php echo !empty($data['tinhthanh']) ? $data['tinhthanh'] : '';?>" >
                        <option>Chọn</option>
                        </select>
                    </div>
                      <div class="form-group col-md-4">
                        <label for="inputState">Quận/huyện</label>
                        <select id="quanhuyen" id="quanhuyen" name="quanhuyen" class="form-control select2 " label=" <?php echo !empty($data['quanhuyen']) ? $data['quanhuyen'] : '';?>">
                        <option>Chọn</option>
                        </select>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputState">Phường/xã</label>
                        <select id="phuongxa" name="phuongxa"  class="form-control select2 " label="<?php echo !empty($data['phuongxa']) ? $data['phuongxa'] : '';?>">
                        <option>Chọn</option>
                        </select>
                      </div>
                       <div class="form-group col-md-4">
                      <label for="inputAddress">Thông tin khác của địa chỉ</label>
                      <input type="text" class="form-control" name="ttkhac" placeholder="số nhà, tên đường,…"  value="<?php echo !empty($data['ttkhac']) ? $data['ttkhac'] : '';?>">
                    </div>
                  
                 </div>
            
               
              </div>
              <!-- /.box-body -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                <h3 class="box-title">Thông tin về đặc điểm bản thân</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="form-group col-md-4">
                    <label for="inputAddress">Tình trạng hôn nhân</label>
                     <select name="honnhan" class="form-control" >
                        <option value="Độc thân" selected>Độc thân</option>
                        <option value="Đã kết hôn" <?php if (!empty($data['honnhan']) && $data['honnhan'] == 'Đã kết hôn') echo 'selected'; ?>> Đã kết hôn</option>
                        <option value="Đã kết hôn" <?php if (!empty($data['honnhan']) && $data['honnhan'] == 'Đã ly hôn') echo 'selected'; ?>>Đã ly hôn</option>
                      </select>
                    </div>
                 
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Chiều cao</label>
                    <input type="text" class="form-control" name="chieucao" placeholder=""  value="<?php echo !empty($data['chieucao']) ? $data['chieucao'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Nhóm máu</label>
                    <input type="text" class="form-control" name="nhommau" placeholder="  O,A,B..."  value="<?php echo !empty($data['nhommau']) ? $data['nhommau'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Thành phần xuất thân</label>
                    <input type="text" class="form-control" name="xuatthan" placeholder="" value="<?php echo !empty($data['xuatthan']) ? $data['xuatthan'] : '';?>">
                  </div>
                   <div class="form-group col-md-4">
                    <label for="inputAddress">sở thích của cá nhân</label>
                    <input type="text" class="form-control" name="sothich" placeholder=""  value="<?php echo !empty($data['sothich']) ? $data['sothich'] : '';?>">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->  
               <div class="box box-primary box-solid">
                <div class="box-header with-border">
                <h3 class="box-title">Thông tin về trình độ, kỹ năng nghề nghiệp</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
               
                <div class="form-group col-md-6">
                    <label for="inputAddress">Trình độ học vấn</label>
                     <input type="text" class="form-control" name="hocvan" placeholder="" value="<?php echo !empty($data['hocvan']) ? $data['hocvan'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-6">
                    <label for="inputAddress">Trình độ ngoại ngữ</label>
                    <input type="text" class="form-control" name="ngoaingu" placeholder="biết những ngoại ngữ nào" value="<?php echo !empty($data['ngoaingu']) ? $data['ngoaingu'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-6">
                    <label for="inputAddress"> Trình độ đào tạo</label>
                    <input type="text" class="form-control" name="daotao" placeholder=" Đại học, Cao đẳng, Trung cấp,…" value="<?php echo !empty($data['daotao']) ? $data['daotao'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-6">
                    <label for="inputAddress">Ngành đào tạo</label>
                    <input type="text" class="form-control" name="nganh" placeholder="Nhập nganh"  value="<?php echo !empty($data['nganh']) ? $data['nganh'] : '';?>">
                  </div>
                </div>
              </div>
                 <div class="box box-primary box-solid">
                <div class="box-header with-border">
                <h3 class="box-title">Quan hệ nhân thân</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body mqh ">
                <div class="moiquanhe ">
                
                  <div class="form-group col-md-3">
                    <label for="inputAddress">Mối quan hệ </label>
                     
                    <input type="text" class="form-control" name="moiquanhe[]" placeholder="Cha, mẹ, vợ,…"  value="<?php echo !empty($data['moiquanhe']) ? $data['moiquanhe'] : '';?>">
                  </div>
                 
                    <div class="form-group col-md-4">
                    <label for="inputAddress"> Họ và tên</label>
                    <input type="text" class="form-control" name="hoten[]" placeholder="Họ và tên"  value="<?php echo !empty($data['hoten']) ? $data['hoten'] : '';?>">
                  </div>
                  
                    <div class="form-group col-md-4">
                    <label for="inputAddress">Địa chỉ liên hệ</label>
                    <input type="text" class="form-control" name="diachi[]" placeholder="Địa chỉ liên hệ"  value="<?php echo !empty($data['diachi']) ? $data['diachi'] : '';?>">
                  </div>
                 
                  <button type="button" id="addmoiquanhe" class="btn plus"><i class="fa fa-plus"></i></button>
          
               
                </div>
             
                
              </div>
            </div>
              <!-- /.box-body --> 
              <a  class="btn btn-info back" href="index.php"> Trở về</a> 
              <button type="submit" id="submit" value="submit" class="btn btn-success save">Lưu</button>
            </form>
         
            <a  class="btn btn-info" href="index.php"> Trở về</a>
      </div>  <!-- /row -->
    
    </section>
 
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->

<script src="dist/js/demo.js"></script>
<script src="dist/js/script.js" type="text/javascript" charset="utf-8" async defer></script>

<script>


</script>
</body>
</html>
